interface Movie {
    title: string,
    poster_path: string,
    id: number,
    backdrop_path: string,
    adult: boolean,
    overview: string,
    release_date: string
}
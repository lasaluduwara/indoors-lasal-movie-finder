import { Component, OnInit } from '@angular/core';

import { MoviesService } from '../movies.service';
@Component({
  selector: 'movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  popularList: Array<Movie>;
  searchRes: Array<Movie>;
  searchStr: string;
  constructor(private _moviesService: MoviesService) {
    this._moviesService.getPopularMovies().subscribe(res => {
      this.popularList = res.results;
    });
  }

  ngOnInit() {
  }

  searchMovies() {
    if (this.searchStr != "" && this.searchStr != undefined) {
      this._moviesService.searchMovies(this.searchStr).subscribe(res => {
        this.searchRes = res.results;
      })
    } else {
      this.searchRes = null;
    }
  }

}

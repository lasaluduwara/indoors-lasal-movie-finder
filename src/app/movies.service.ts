import { Injectable } from '@angular/core';
import { environment } from "../environments/environment";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs/Rx';

@Injectable()
export class MoviesService {
  apikey: string;

  constructor(private httpClient: HttpClient) {
  }

  getPopularMovies(): Observable<Pagination> {
    return this.httpClient.get<Pagination>(`${environment.MOVIE_API}movie/popular?api_key=${environment.API_KEY}&language=${environment.LANGUAGE}`)
  }

  searchMovies(searchStr: string): Observable<Pagination> {
    return this.httpClient.get<Pagination>(`${environment.MOVIE_API}search/movie?api_key=${environment.API_KEY}&language=${environment.LANGUAGE}&query=${searchStr}`)
  }

  getMovie(id: string): Observable<Movie> {
    return this.httpClient.get<Movie>(`${environment.MOVIE_API}movie/${id}?api_key=${environment.API_KEY}&language=${environment.LANGUAGE}`)
  }
}

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  //MOVIE_API:"https://api.themoviedb.org/3/search/movie?api_key=1a1d14583e8568ffba2d3058da215f7c&language=en-US",
  MOVIE_API:"https://api.themoviedb.org/3/",
  API_KEY:"1a1d14583e8568ffba2d3058da215f7c",
  LANGUAGE: "en-US"
};
